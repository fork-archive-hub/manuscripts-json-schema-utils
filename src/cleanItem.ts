/*!
 * © 2019 Atypon Systems LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { ModelAttachment } from '@manuscripts/manuscript-transform'
import { Model, ObjectTypes } from '@manuscripts/manuscripts-json-schema'

export interface JsonModel extends Model, ModelAttachment {
  bundled?: boolean
  collection?: string
  contentType?: string
}

// TODO: merge into one function?

/* eslint-disable @typescript-eslint/no-unused-vars */

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const funcs: Array<(item: any) => JsonModel> = [
  // Remove duplicate id field from all items
  item => {
    const { id, _id } = item

    if (id) {
      if (!_id) {
        item._id = id
      }

      delete item.id
    }

    return item
  },
  // Remove bundled and locked from all items
  ({ bundled, locked, ...item }) => item,
  // Remove derived fields from Contributors
  item => {
    if (item.objectType === ObjectTypes.Contributor) {
      const {
        firstName,
        fullName,
        lastName,
        middleNames,
        nameString,
        prename,
        ...rest
      } = item
      return rest
    } else {
      return item
    }
  },
  // Remove caption field from Table
  item => {
    if (item.objectType === ObjectTypes.Table) {
      const { caption, ...rest } = item
      return rest
    } else {
      return item
    }
  },
  // Remove _rev and collection from all items
  ({ _rev, collection, ...item }) => item,
  // Rename containingElement to containingObject with Citation items
  item => {
    if (item.objectType === ObjectTypes.Citation) {
      const { containingElement, containingObject, ...rest } = item
      if (
        containingObject &&
        containingElement &&
        containingObject !== containingElement
      ) {
        throw new Error('Mismatching container values for Citation')
      }
      return {
        ...rest,
        containingObject: containingElement,
      }
    } else {
      return item
    }
  },
  // Set a default type for BibliographyItems
  item => {
    if (item.objectType === ObjectTypes.BibliographyItem && !item.type) {
      item.type = 'article-journal'
    }
    return item
  },
  // Clean up author/embeddedAuthors in BibliographyItems
  item => {
    if (item.objectType === ObjectTypes.BibliographyItem) {
      const { author, embeddedAuthors } = item
      if (embeddedAuthors) {
        if (!author) {
          item.author = embeddedAuthors.slice()
        }
        delete item.embeddedAuthors
      }
    }
    return item
  },
]

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const cleanItem = (item: any): JsonModel => {
  for (const f of funcs) {
    item = f(item)
  }

  return item
}
